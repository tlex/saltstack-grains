# -*- coding: utf-8 -*-
"""
Grains for the github API

The grains relay on YAML configuration files located in `/etc/saltstack-grains/*.yaml`.

The following options are supported:
```
github:
  url: https://api.github.com # optional
  token: your_token # optional
  repositories:
    - user1/repository1
    - user2/repository2
```
You can omit `url` entirely and the default will be used (https://api.github.com).

If `token` is omitted, an unauthenticated request will be made. Create one here: https://github.com/settings/tokens
(no special/additional permissions are required). Read about rate-limiting:
https://developer.github.com/v3/#rate-limiting

The `repositories` keys will be merged from all the `*.yaml` files found.

The github.com API reference is available here: https://developer.github.com/v3/

The following grains are returned about each configured repository:
github:
  latest_release:
    tag_name: Boolean True/False - False if one or more items are playing in plex
    prerelease: Boolean True/False - The release is/isn't a prerelease
    draft: Boolean True/False - The release is/isn't a draft
"""
import logging
import requests
import os
import glob
import json
import yaml

log = logging.getLogger(__name__)

settings = {
    'url': 'https://api.github.com',
    'token': None,
    'repositories': [],
}

headers = {
    'User-Agent': 'saltstack grain scraper'
}


def _update_settings():
    """
    Dynamically loads the files in /etc/saltstack-grains/*.yaml

    @return nil
    """
    config_files = glob.glob('/etc/saltstack-grains/*.yaml')

    for config_file in config_files:
        if os.path.isfile(config_file):
            with open(config_file, 'r') as ymlfile:
                cfg = yaml.load(ymlfile)
                for k in settings:
                    if 'github' in cfg and k in cfg['github']:
                        if k in ['url', 'token']:
                            settings[k] = cfg['github'][k]
                        if k == 'repositories' and type(cfg['github'][k]) is list:
                            for repo in cfg['github'][k]:
                                if repo not in settings['repositories']:
                                    settings['repositories'].append(repo)

    log.trace('Loaded settings: {0}'.format(settings))


def _get_latest_release(repository):
    """
    Connects to github and gets the latest release data

    @return mixed dict with the information or boolean False if the connection failed
    """
    url = '{}/{}/{}/{}'.format(settings['url'], 'repos', repository, 'releases/latest')
    headers = {}
    if settings.get('token'):
        headers = {
            'Authorization': 'token {}'.format(settings['token'])
        }
    response = False
    log.trace('Creating request to {}'.format(url))
    try:
        response = requests.get(url, headers=headers).json()
    except Exception as e:
        log.warning('Could not retrieve the data from github. The Exception follows:')
        log.warning(msg='{0}'.format(e))

    if response and 'tag_name' in response:
        result = {
            'tag_name': response['tag_name'],
            'prerelease': response['prerelease'],
            'draft': response['draft']
        }
        return result

    return False


def github():
    """
    The public function called by saltstack

    @return dict the discovered grains
    """
    _update_settings()

    # Safe defaults
    grains = {
        'github': {}
    }

    # Only run if there are repositories configured
    if type(settings['repositories']) is list and len(settings['repositories']) > 0:
        for repository in settings['repositories']:
            grains['github'].update({
                repository: {
                    'latest_release': _get_latest_release(repository=repository)
                },
            })
        log.debug('{0}: {1}'.format(os.path.basename(__file__), grains))
        return grains
