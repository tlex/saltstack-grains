# -*- coding: utf-8 -*-
"""
The grains for the plex media server (https://plex.tv)

The grains relay on a YAML configuration file located in `/etc/plex_version.yaml`. For the supported options see
`plex_version.yaml.example`.
The following grains are returned:
plex:
  installed: Boolean True/False - False if there is no config file in `/etc/plex_version.yaml`
  safe_to_update: Boolean True/False - False if one or more items are playing in plex
  version_available: Mixed - string (the available version for download) / boolean False if plex_installed is False
  version_installed: Mixed - string (the installed version) / boolean False if the connection to the plex server failed
"""
import logging
import requests
import os
import json
import xml.etree.ElementTree as ET
import yaml

log = logging.getLogger(__name__)

settings = {
    'token': False,
    'plex_url': 'https://plex.tv/api/downloads/1.json',
    'type': 'free',
    'server': 'localhost',
    'port': 32400,
    'ssl': True,
    'verify_ssl': False,
    'plex_xml_config_file': '/var/lib/plexmediaserver/Library/Application Support/Plex Media Server/Preferences.xml',
    'plex_installed': False,
    'scrape_hidden_settings': False,
    'timeout': 2,
}

headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:52.0) Gecko/20100101 Firefox/52.0'
}


def _update_settings():
    """
    Reads the configuration file in /etc/plex_version.yaml and overwrites the defaults

    @return nil
    """
    config_file = '/etc/plex_version.yaml'

    if os.path.isfile(config_file):
        with open(config_file, 'r') as ymlfile:
            cfg = yaml.load(ymlfile)
            for k in settings:
                if 'plex_version' in cfg and k in cfg['plex_version']:
                    settings[k] = cfg['plex_version'][k]

        # TODO: actually detect if plex is installed or not
        settings['plex_installed'] = True

    if not settings['token']:
        # Find the PlexOnlineToken from the config file
        if os.path.isfile(settings['plex_xml_config_file']):
            try:
                settings['token'] = ET.parse(settings['plex_xml_config_file']).getroot().attrib['PlexOnlineToken']
            except Exception as e:
                log.warning('Could not get the plex token. The Exception follows:')
                log.warning(msg='{0}'.format(e))

    log.trace('Loaded settings: {0}'.format(settings))


def _get_available_plex_version():
    """
    Connects to plex.tv and reads the latest available version

    @return mixed string the available version or boolean False if the connection failed
    """
    if settings['type'] == 'pass':
        settings['plex_url'] = '{0}?channel=plexpass'.format(settings['plex_url'])
        # Without a token it will just revert to the normal free (non pass) results
        if settings['token']:
            headers['X-Plex-Token'] = settings['token']

    try:
        return requests.get(
            settings['plex_url'],
            headers=headers,
            timeout=settings.get('timeout')
        ).json()['computer']['Linux']['version']
    except Exception as e:
        log.warning('Could not detect the latest Plex version. The Exception follows:')
        log.warning(msg='{0}'.format(e))
    return False


def _get_installed_plex_version():
    """
    Connects (unauthenticated) to the plex server and reads the version

    @return mixed string the installed version or boolean False if the connection failed
    """
    pre = 'https' if settings['ssl'] else 'http'
    url = '{0}://{1}:{2}/identity'.format(
            pre,
            settings['server'],
            settings['port']
    )

    r = False
    try:
        # The request
        r = requests.get(url, verify=settings['verify_ssl'], headers=headers, timeout=settings.get('timeout'))
    except Exception as e:
        log.warning('Could not connect to the plex server. The Exception follows:')
        log.warning(msg='{0}'.format(e))
    if r:
        return ET.fromstring(r.text).attrib['version']

    # Else just return false
    return False


def _get_plex_idle():
    """
    Connects to the plex server and gets the playback status

    @return boolean True if items are playing / False if the connection failed or no items are playing
    """
    if settings['token']:
        headers['X-Plex-Token'] = settings['token']
        pre = 'https' if settings['ssl'] else 'http'
        url = '{0}://{1}:{2}/status/sessions'.format(
                pre,
                settings['server'],
                settings['port']
        )

        r = False
        try:
            # The request
            r = requests.get(url, verify=settings['verify_ssl'], headers=headers, timeout=settings.get('timeout'))
        except Exception as e:
            log.warning('Could not connect to the plex server. The Exception follows:')
            log.warning(msg='{0}'.format(e))

        if r:
            playing = ET.fromstring(r.text).attrib['size']
            if playing != '0':
                log.warning('Not safe to update. There are {0} items playing.'.format(playing))
                return False

    # Else just assume it's safe to update
    return True


def _get_plex_settings():
    """
    Connects to the plex server and gets information from the configured settings

    @return mixed dict with settings / boolean False if the connection failed
    """
    if settings['token']:
        headers['X-Plex-Token'] = settings['token']
        pre = 'https' if settings['ssl'] else 'http'
        url = '{0}://{1}:{2}/:/prefs'.format(
                pre,
                settings['server'],
                settings['port']
        )

    r = False
    try:
        r = requests.get(url, verify=settings['verify_ssl'], headers=headers, timeout=settings.get('timeout'))
    except Exception as e:
        log.warning('Could not connect to the plex server. The Exception follows:')
        log.warning(msg='{0}'.format(e))

    if r:
        results = ET.fromstring(r.text)
        plex_settings = {}
        for child in results:
            group = child.attrib['group'] if len(child.attrib['group']) > 0 else 'nogroup'
            if child.attrib['hidden'] == '0' or settings['scrape_hidden_settings']:
                if group not in plex_settings:
                    plex_settings.update({group: {}})
                value = child.attrib['value']
                if str(value).lower() == 'true' or (value == '1' and child.attrib['type'] == 'bool'):
                    value = True
                if str(value).lower() == 'false' or (value == '0' and child.attrib['type'] == 'bool'):
                    value = False
                if child.attrib['type'] == 'int':
                    value = int(value)
                plex_settings[group].update({child.attrib['id']: value})
        return plex_settings
    return False


def plex_version():
    """
    The public function called by saltstack

    @return dict the discovered grains
    """
    _update_settings()

    # Safe defaults
    grains = {
        'plex': {
            'installed': settings['plex_installed'],
            'version_available': False,
            'version_installed': False,
            'safe_to_update': False,
        }
    }

    # Only run this if plex is installed
    if settings['plex_installed']:
        grains['plex']['safe_to_update'] = _get_plex_idle()
        grains['plex']['version_available'] = _get_available_plex_version()
        grains['plex']['version_installed'] = _get_installed_plex_version()
        grains['plex']['settings'] = _get_plex_settings()

    log.debug('{0}: {1}'.format(os.path.basename(__file__), grains))
    return grains
