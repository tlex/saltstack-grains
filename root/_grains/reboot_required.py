# -*- coding: utf-8 -*-
'''
Simple grain to check if reboot is required after an update

It works on Debian and flavours of Debian (ex. Ubuntu), by checking for the presence of one of these files:
- /run/reboot-required
- /run/reboot-required.pkg
- /var/run/reboot-required.pkg
- /var/run/reboot-required.pkg

'''

import logging
import os.path

log = logging.getLogger(__name__)


def reboot_required():
    grains = {'reboot_required': False}

    files = [
        '/run/reboot-required',
        '/run/reboot-required.pkg',
        '/var/run/reboot-required',
        '/var/run/reboot-required.pkg'
    ]

    for file in files:
        if os.path.exists(file):
            grains['reboot_required'] = True
            log.info('File {0} exists. Reboot required.'.format(file))

    log.debug('{0}: {1}'.format(os.path.basename(__file__), grains))

    return grains
