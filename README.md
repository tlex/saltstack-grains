# SaltStack Grains

Custom grains to be used with [https://saltstack.com](SaltStack).

## Contents
*   [plex_version](#plex_version)
    *   [Configuration](#configuration)
    *   [Grains](#grains)
    *   [Installation](#installation)
    *   [Example](#example)
*   [reboot_required](#reboot_required)
    *   [Example](#example-1)
*   [github](#github)
    *   [Configuration](#configuration-1)
    *   [Grains](#grains-1)
    *   [Installation](#installation-1)
    *   [Example](#example-2)
*   [Repository Usage](#repository-usage)
    *   [Use this Repository Directly](#use-this-repository-directly)
    *   [Manual Installation](#manual-installation)

## plex_version

[plex_version.py](root/_grains/plex_version.py): Grains for [plex media server](https://plex.tv).

### Configuration
The grains rely on a YAML configuration file located under `/etc/plex_version.yaml`. If the file isn't there, all the
grains are set to `False` and it will **not** check against [Plex](https://plex.tv) for the latest version. For the
supported options see [plex_version.example.yaml](config-examples/plex_version.example.yaml).

### Grains
The following grains are exported:
```yaml
plex:
  installed: Boolean True/False - False if there is no config file in `/etc/plex_version.yaml`
  version_available: Mixed - string (the latest version available for download) / boolean False
  version_installed: Mixed - string (the version installed) / boolean False if the connection doesn't work
  safe_to_update: Boolean True/False - False if one or more items are playing in plex
  settings: Mixed dict with the settings configured in your Plex Media Server / boolean False
```

### Installation
Don't forget to create the configuration file `/etc/plex_version.yaml`.

### Example
```
root@server_salt_master:~# salt -C 'G@plex:installed:True' grains.get plex
server_plex.domain:
  ----------
  installed:
      True
  settings:
      ----------
      butler:
          ----------
          ButlerDatabaseBackupPath:
              /var/lib/plexmediaserver/backup/
          ButlerEndHour:
              8
          ButlerStartHour:
              1
          ButlerTaskBackupDatabase:
              True
          ButlerTaskUpgradeMediaAnalysis:
              True
      channels:
          ----------
          disableCapabilityChecking:
              False
          iTunesLibraryXmlPath:
          iTunesSharingEnabled:
              False
      dlna:
          ----------
          DlnaAnnouncementLeaseTime:
              1800
          DlnaClientPreferences:
          DlnaDefaultProtocolInfo:
              http-get:*:video/mpeg:*,http-get:*:video/mp4:*,http-get:*:video/vnd.dlna.mpeg-tts:*,http-get:*:video/avi:*,http-get:*:video/x-matroska:*,http-get:*:video/x-ms-wmv:*,http-get:*:video/wtv:*,http-get:*:audio/mpeg:*,http-get:*:audio/mp3:*,http-get:*:audio/mp4:*,http-get:*:audio/x-ms-wma*,http-get:*:audio/wav:*,http-get:*:audio/L16:*,http-get:*image/jpeg:*,http-get:*image/png:*,http-get:*image/gif:*,http-get:*image/tiff:*
          DlnaDescriptionIcons:
              png,jpeg;260x260,120x120,48x48
          DlnaDeviceDiscoveryInterval:
              60
          DlnaEnabled:
              False
          DlnaReportTimeline:
              True
      extras:
          ----------
          CinemaTrailersFromBluRay:
              True
          CinemaTrailersFromLibrary:
              False
          CinemaTrailersFromTheater:
              True
          CinemaTrailersPrerollID:
          CinemaTrailersType:
              1
      general:
          ----------
          ButlerTaskCheckForUpdates:
              True
          ButlerUpdateChannel:
              8
          FriendlyName:
              media
          LogVerbose:
              True
          collectUsageData:
              False
          logDebug:
              True
          logTokens:
              False
      library:
          ----------
          FSEventLibraryPartialScanEnabled:
              True
          FSEventLibraryUpdatesEnabled:
              True
          GenerateBIFBehavior:
              asap
          GenerateChapterThumbBehavior:
              scheduled
          OnDeckWindow:
              16
          ScannerLowPriority:
              True
          ScheduledLibraryUpdateInterval:
              3600
          ScheduledLibraryUpdatesEnabled:
              True
          allowMediaDeletion:
              True
          autoEmptyTrash:
              True
          watchMusicSections:
              False
      network:
          ----------
          EnableIPv6:
              True
          GdmEnabled:
              False
          LanNetworksBandwidth:
              127.0.0.1
          WanPerUserStreamCount:
              0
          WebHooksEnabled:
              True
          allowedNetworks:
              127.0.0.1/255.255.255.255
          customCertificateDomain:
              **redacted**
          customCertificateKey:
              **redacted**
          customCertificatePath:
              /var/lib/plexmediaserver/**redacted**.p12
          customConnections:
              https://**redacted**
          enableHttpPipelining:
              True
          secureConnections:
              1
      transcoder:
          ----------
          SegmentedTranscoderTimeout:
              20
          TranscodeCountLimit:
              0
          TranscoderDefaultDuration:
              120
          TranscoderH264BackgroundPreset:
              ultrafast
          TranscoderQuality:
              2
          TranscoderTempDirectory:
              /var/lib/plexmediaserver/transcoder/
          TranscoderThrottleBuffer:
              60
  safe_to_update:
      False
  version_available:
      1.5.1.3520-ed60c70d6
  version_installed:
      1.5.1.3520-ed60c70d6
```

## reboot_required

[reboot_required.py](root/_grains/reboot_required.py): A grain to let you know when a server needs to reboot after an update. Tested with Ubuntu, but it should work on all
Debian flavours.

#### Example
```
root@server_salt_master:~# salt '*' grains.get reboot_required
server1.domain:
    True
server2.domain
    False
root@server_salt_master:~# salt -C 'G@reboot_required:True and G@location:nl-ams-01' system.reboot
```

<a name="github"></a>

## github

[github.py](root/_grains/github.py): Grains for [github](https://github.com).

### Configuration
The grains rely on YAML configuration files located under `/etc/saltstack-grains/*.yaml`. If no yaml file is found, or
if no `repositories` are defined, the grains will not be exported (and **no** request will be sent to the github API).
For the supported options see [github.example.yaml](config-examples/gihub.example.yaml).

### Grains
The following grains are exported:
```yaml
github:
  {{ user }}/{{ repository}}:
    latest_release:
      draft: Boolean
      prerelease: Boolean
      tag_name: String
```

### Installation
Don't forget to create the configuration file `/etc/saltstack-grains/*.yaml`.

### Example
```
root@server_salt_master:~# salt -G 'roles:prometheus' grains.get github
server_prometheus.domain:
----------
prometheus/alertmanager:
    ----------
    latest_release:
        ----------
        draft:
            False
        prerelease:
            False
        tag_name:
            v0.5.1
prometheus/prometheus:
    ----------
    latest_release:
        ----------
        draft:
            False
        prerelease:
            False
        tag_name:
            v1.6.0
```

## Repository Usage

### Use this Repository Directly
Add the following to your salt-master config:
```yaml
gitfs_remotes:
  - https://gitlab.com/tlex/saltstack-grains.git:
    - mountpoint: salt://
    - root: root
    - base: v1.1
```

**Warning:** Always use a git tag for your base! This way you can control the update yourself. Of course, I recommend
using the latest tag available.

### Manual Installation
Copy the python script file(s) in `_grains/` in one of your `file_root` folders. For details see
[Writing Grains](https://docs.saltstack.com/en/latest/topics/grains/#writing-grains).
